export default (_, res, next) => {
  res.sendOk = (payload = undefined) => res.status(200).send(payload)
  res.sendCreated = (payload = undefined) => res.status(201).send(payload)
  res.sendNoContent = () => res.status(204).send()
  res.sendBadRequest = (payload = undefined) => res.status(400).send(payload)
  res.sendNotFound = (payload = undefined) => res.status(404).send(payload)
  res.sendError = (payload = undefined) => res.status(500).send(payload)

  next()
}
