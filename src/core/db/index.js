import Sequelize from 'sequelize'
import { DB_HOST, isTest } from '../../config'

const DB_NAME = isTest() ? 'tapago_test' : 'tapago'
const DB_ADDR = isTest() ? 'localhost:25432' : DB_HOST

const logging = isTest() ? false : console.log

const options = {
  logging,
  pool: {
    min: 0,
    max: 5,
    idle: 1000
  }
}

const sequelize = new Sequelize(`postgres://postgres@${DB_ADDR}/${DB_NAME}`, options)

const force = isTest()

// mocha injects this function when the delay option is enabled,
// so we can run async tasks before actually run the test suite
const run = global.run || (() => {})

sequelize.sync({ force }).then(() => run())

export default sequelize
