export default class BaseService {
  constructor(model) {
    this.model = model
  }

  async all() {
    return this.model.findAll()
  }

  async get(entryId) {
    return this.model.findByPk(entryId)
  }

  async create(body) {
    return this.model.create(body)
  }

  async update(entryId, body) {
    const entry = await this.get(entryId)

    return entry.update(body)
  }

  async delete(entryId) {
    const entry = await this.get(entryId)

    return entry.destroy()
  }
}
