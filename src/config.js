export const isTest = () => process.env.NODE_ENV.toLowerCase() === 'test'

export const DB_HOST = process.env.DB_HOST || 'localhost:5432'
export const PORT = isTest() ? 5001 : process.env.PORT || 5000
