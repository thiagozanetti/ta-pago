import express from 'express'

import methods from '../core/middlewares/methods'
import v1 from './1'

const api = express()

api.use(methods)
api.use('/1', v1)

export default api
