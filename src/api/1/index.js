import express from 'express'
import transactions from './transactions'
import payables from './payables'

const v1 = express()

v1.use('/transactions', transactions)
v1.use('/payables', payables)

export default v1
