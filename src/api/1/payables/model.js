import Sequelize from 'sequelize'
import moment from 'moment'

import sequelize from '../../../core/db'

const FEE_TO_APPLY = {
  paid: 3,
  waiting_funds: 5
}

class Payable extends Sequelize.Model {}

Payable.init({
  status: {
    type: Sequelize.ENUM('paid', 'waiting_funds'),
    required: true,
    allowNull: false
  },
  total: {
    type: Sequelize.DECIMAL(10, 2),
    required: true,
    allowNull: false,
    set(value) {
      const status = this.getDataValue('status')
      const fee = FEE_TO_APPLY[status]

      const finalValue = value - ((value * fee) / 100)

      this.setDataValue('total', finalValue)
    }
  },
  paymentDate: {
    type: Sequelize.DATEONLY,
    required: true,
    allowNull: false,
    set(value) {
      const status = this.getDataValue('status')

      let finalDate = moment(value)

      if (status === 'waiting_funds') {
        finalDate = moment(value).add(30, 'days')
      }

      this.setDataValue('paymentDate', finalDate.toDate())
    }
  }
}, { sequelize })

export default Payable
