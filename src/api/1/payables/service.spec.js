import { describe, it } from 'mocha'
import { expect } from 'chai'
import moment from 'moment'

import PayableService from './service'

export const paymentDate = moment().toDate()

export const payablePayload = {
  status: 'paid',
  total: 100.00,
  paymentDate
}

describe('Payables Service', async () => {
  it('must create a new payable as paid', async () => {
    // given
    const ps = new PayableService()

    const expected = { ...payablePayload, total: '97.00', paymentDate: moment(paymentDate).format('YYYY-MM-DD') }

    // when
    const actual = await ps.create(payablePayload)

    // then
    expect(actual.dataValues).to.be.shallowDeepEqual(expected)
  })

  it('must create a new payable as waiting_funds', async () => {
    // given
    const ps = new PayableService()

    const expected = { ...payablePayload, status: 'waiting_funds', total: '95.00', paymentDate: moment(paymentDate).add(30, 'days').format('YYYY-MM-DD') }

    // when
    const actual = await ps.create({ ...payablePayload, status: 'waiting_funds' })

    // then
    expect(actual.dataValues).to.be.shallowDeepEqual(expected)
  })

  it('must throw trying to create a new payable with missing required values', async () => {
    // given
    let actual

    try {
      // when
      const ps = new PayableService()

      actual = await ps.create()
    } catch (error) {
      // then
      expect(error.message).to.be.equal('notNull Violation: Payable.status cannot be null,\nnotNull Violation: Payable.total cannot be null,\nnotNull Violation: Payable.paymentDate cannot be null')
    }

    expect(actual).to.be.undefined()
  })

  it('must throw trying to create a new payable with wrong status', async () => {
    // given
    let actual

    try {
      // when
      const ps = new PayableService()

      actual = await ps.create({ ...payablePayload, status: 'wrong_status' })
    } catch (error) {
      // then
      expect(error.message).to.be.equal('invalid input value for enum "enum_Payables_status": "wrong_status"')
    }

    expect(actual).to.be.undefined()
  })

  // https://github.com/brianc/node-postgres/pull/1992#issuecomment-543447322
  it.skip('must display the summary of the total available per status', async () => {
    // given
    const ps = new PayableService()

    const PAYABLE_LIST = [['paid', 100.00], ['paid', 150.00], ['paid', 200.00], ['waiting_funds', 250.00], ['waiting_funds', 300.00], ['waiting_funds', 350.00]]

    await Promise.all(PAYABLE_LIST.map(async ([status, total]) => ps.create({ ...payablePayload, status, total })))

    const expected = [
      { status: 'paid', total: '100.00' },
      { status: 'waiting_funds', total: '100.00' }
    ]

    // when
    // this is causing the issue with calling pg-native for some reason. Works in prod mode
    const actual = await ps.summary()

    // then
    expect(actual).to.be.shallowDeepEqual(expected)
  })
})
