import { Router } from 'express'
import controller from './controller'

const payables = Router()

payables.route('/')
  .get(controller.all)

payables.route('/:payableId(\\d+)')
  .get(controller.get)

payables.route('/summary')
  .get(controller.summary)

export default payables
