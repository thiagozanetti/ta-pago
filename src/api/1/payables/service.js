import BaseService from '../../../core/services/base'
import Payable from './model'
import sequelize from '../../../core/db'

export default class PayableService extends BaseService {
  constructor(model = Payable) {
    super(model)
  }

  async summary() {
    return this.model.findAll({
      attributes: [
        [sequelize.literal('CASE status WHEN \'paid\' THEN \'available\' ELSE \'waiting_funds\' END'), 'status'],
        [sequelize.fn('SUM', sequelize.col('total')), 'total']],
      group: 'status'
    })
  }
}
