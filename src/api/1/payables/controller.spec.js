import { describe } from 'mocha'
import { expect } from 'chai'

import { requester, addTransaction } from '../transactions/controller.spec'

import { transactionPayload } from '../transactions/service.spec'

const getAllPayables = async () => requester.get('/api/1/payables').send()
const getPayable = async payableId => requester.get(`/api/1/payables/${payableId}`).send()
const getPayableSummary = async () => requester.get('/api/1/payables/summary').send()

// TODO: add better assertions for this entire suite
describe('Payables API', () => {
  it('must get all payables', async () => {
    const addTransactionResponse = await addTransaction(transactionPayload)

    expect(addTransactionResponse).to.have.status(201)

    const getPayableResponse = await getAllPayables()

    expect(getPayableResponse).to.have.status(200)
  })

  it('must get a given payable', async () => {
    const addTransactionResponse = await addTransaction(transactionPayload)

    expect(addTransactionResponse).to.have.status(201)

    const { body: { id } } = addTransactionResponse

    const getPayableResponse = await getPayable(id)

    expect(getPayableResponse).to.have.status(200)
  })

  it('must get the payable summary', async () => {
    const addTransactionResponse = await addTransaction(transactionPayload)

    expect(addTransactionResponse).to.have.status(201)

    const getPayableSummaryResponse = await getPayableSummary()

    expect(getPayableSummaryResponse).to.have.status(200)
  })
})
