import PayableService from './service'

export default {
  async all(_, res) {
    try {
      const ps = new PayableService()

      const result = await ps.all()

      return res.sendOk(result)
    } catch ({ message: error }) {
      res.sendError({ error })
    }
  },
  async get(req, res) {
    try {
      const { params: { payableId } } = req

      const ps = new PayableService()

      const result = await ps.get(payableId)

      return res.sendOk(result)
    } catch ({ message: error }) {
      res.sendError({ error })
    }
  },
  async summary(_, res) {
    try {
      const ps = new PayableService()

      const result = await ps.summary()

      return res.sendOk(result)
    } catch ({ message: error }) {
      res.sendError({ error })
    }
  }
}
