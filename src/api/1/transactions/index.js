import { Router } from 'express'
import controller from './controller'

const transactions = Router()

transactions.route('/')
  .get(controller.all)
  .post(controller.create)

transactions.route('/:transactionId(\\d+)')
  .get(controller.get)

export default transactions
