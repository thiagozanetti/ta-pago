import BaseService from '../../../core/services/base'
import Transaction from './model'

export default class TransactionService extends BaseService {
  constructor(model = Transaction) {
    super(model)
  }
}
