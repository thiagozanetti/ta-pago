import Sequelize from 'sequelize'

import sequelize from '../../../core/db'

class Transaction extends Sequelize.Model {}

Transaction.init({
  description: {
    type: Sequelize.STRING,
    required: true,
    allowNull: false
  },
  amount: {
    type: Sequelize.DECIMAL(10, 2),
    required: true,
    allowNull: false
  },
  method: {
    type: Sequelize.ENUM('debit_card', 'credit_card'),
    required: true,
    allowNull: false
  },
  cardHolder: {
    type: Sequelize.STRING,
    required: true,
    allowNull: false
  },
  cardNumber: {
    type: Sequelize.STRING,
    required: true,
    allowNull: false,
    get() {
      return `xxxx-xxxx-xxxx-${this.getDataValue('cardNumber')}`
    },
    set(value) {
      this.setDataValue('cardNumber', value.slice(-4))
    }
  },
  cardValidity: {
    type: Sequelize.STRING,
    required: true,
    allowNull: false
  },
  cardCVV: Sequelize.STRING
}, { sequelize })

export default Transaction
