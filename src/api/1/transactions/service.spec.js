import { describe, it } from 'mocha'
import { expect } from 'chai'

import TransactionService from './service'

export const transactionPayload = {
  description: 'product #1',
  amount: 100.00,
  method: 'debit_card',
  cardHolder: 'Juarez Garcia',
  cardNumber: '4004-2345-0987-0129',
  cardValidity: '12/23',
  cardCVV: '610'
}

describe('Transactions Service', () => {
  it('must create a new transaction', async () => {
    // given
    const ts = new TransactionService()

    const expected = { ...transactionPayload, cardNumber: '0129' }

    // when
    const actual = await ts.create(transactionPayload)

    // then
    expect(actual.dataValues).to.be.shallowDeepEqual(expected)
  })

  it('must throw trying to create a new transaction with missing required values', async () => {
    // given
    let actual

    try {
      // when
      const ts = new TransactionService()

      actual = await ts.create()
    } catch (error) {
      // then
      expect(error.message).to.be.equal('notNull Violation: Transaction.description cannot be null,\nnotNull Violation: Transaction.amount cannot be null,\nnotNull Violation: Transaction.method cannot be null,\nnotNull Violation: Transaction.cardHolder cannot be null,\nnotNull Violation: Transaction.cardNumber cannot be null,\nnotNull Violation: Transaction.cardValidity cannot be null')
    }

    expect(actual).to.be.undefined()
  })

  it('must throw trying to create a new transaction with wrong method', async () => {
    // given
    let actual

    try {
      // when
      const ts = new TransactionService()

      actual = await ts.create({ ...transactionPayload, method: 'wrong_method' })
    } catch (error) {
      // then
      expect(error.message).to.be.equal('invalid input value for enum "enum_Transactions_method": "wrong_method"')
    }

    expect(actual).to.be.undefined()
  })

  it('must mask the card number and only display the last 4 numbers', async () => {
    // given
    const ts = new TransactionService()

    const expected = 'xxxx-xxxx-xxxx-0129'

    const { dataValues: { id } } = await ts.create(transactionPayload)

    // when
    const actual = await ts.get(id)

    // then
    expect(actual.cardNumber).to.be.equal(expected)
  })
})
