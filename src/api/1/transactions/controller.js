import TransactionService from './service'
import PayableService from '../payables/service'
import moment from 'moment'

const STATUS_PER_METHOD = {
  debit_card: 'paid',
  credit_card: 'waiting_funds'
}

export default {
  async all(_, res) {
    try {
      const ts = new TransactionService()

      const result = await ts.all()

      return res.sendOk(result)
    } catch ({ message: error }) {
      res.sendError({ error })
    }
  },
  async get(req, res) {
    try {
      const { params: { transactionId } } = req

      const ts = new TransactionService()

      const result = await ts.get(transactionId)

      return res.sendOk(result)
    } catch ({ message: error }) {
      res.sendError({ error })
    }
  },
  async create(req, res) {
    try {
      const { body } = req

      const ts = new TransactionService()

      const result = await ts.create(body)

      const ps = new PayableService()

      await ps.create({
        status: STATUS_PER_METHOD[body.method],
        total: body.amount,
        paymentDate: moment().toDate()
      })

      return res.sendCreated(result)
    } catch ({ message: error }) {
      res.sendError({ error })
    }
  }
}
