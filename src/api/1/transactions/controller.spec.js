import { describe } from 'mocha'
import { expect, request } from 'chai'

import { transactionPayload } from './service.spec'

import service from '../../../'
import sequelize from '../../../core/db'

export const requester = request(service).keepOpen()

export const addTransaction = async body => requester.post('/api/1/transactions').send(body)
const getTransaction = async transactionId => requester.get(`/api/1/transactions/${transactionId}`).send()

// TODO: add better assertions for this entire suite
describe('Transactions API', () => {
  it('must add a new transaction', async () => {
    const addTransactionResponse = await addTransaction(transactionPayload)

    expect(addTransactionResponse).to.have.status(201)
  })

  it('must get a given transaction', async () => {
    const addTransactionResponse = await addTransaction(transactionPayload)
    expect(addTransactionResponse).to.have.status(201)

    const { body: { id } } = addTransactionResponse

    const getTransactionResponse = await getTransaction(id)

    expect(getTransactionResponse).to.have.status(200)
  })
})

after(async () => {
  await sequelize.close()
  requester.close()
})
