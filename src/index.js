import express from 'express'
import bodyParser from 'body-parser'

import api from './api'
import { PORT } from './config'

const service = express()

service.use(bodyParser.json())

service.use('/api', api)

service.listen(PORT, () => {
  console.log(`service is listening to port ${PORT}`)
})

export default service
