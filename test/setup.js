import chai from 'chai'
import chaiHttp from 'chai-http'
import chaiShallowDeepEqual from 'chai-shallow-deep-equal'
import dirtyChai from 'dirty-chai'

chai.use(chaiHttp)
chai.use(chaiShallowDeepEqual)

// https://github.com/standard/standard/issues/690#issuecomment-278533482
chai.use(dirtyChai)
