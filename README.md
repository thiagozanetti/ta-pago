# ta-pago
#TAPAGO - CODE ASSESSMENT

## HOW TO RUN

first of all, make sure you have docker and docker-compose properly installed, then:

```sh
docker-compose up -d --build
```
The server will listen to port 5000 and the Postgres instance will listen to port 25432 (for running tests)

To shutdown the servers and clean up the containers:

```
docker-compose down
```

If you do prefer run it locally, make sure to you have a proper PostgresSQL installed and both `tapago` and `tapago_test` databases created. You gonna need node v12 installed as well.

## API

### TRANSACTIONS

#### ADD A NEW TRANSACTION

```
curl -X POST http://localhost:5000/api/1/transactions/ \
  -d '{
  "description": "Big Mac",
  "amount": 32.00,
  "method": "debit_card",
  "cardHolder": "Ronald McDonalds",
  "cardNumber": "0098-8923-6543-2345",
  "cardValidity": "2019-12-16",
  "cardCVV": "381"
}'
```

```
STATUS: 201

{
    "cardNumber": "xxxx-xxxx-xxxx-2345",
    "id": 8,
    "description": "Big Mac",
    "amount": "32.00",
    "method": "debit_card",
    "cardHolder": "Ronald McDonalds",
    "cardValidity": "2019-12-16",
    "cardCVV": "381",
    "updatedAt": "2019-12-19T04:18:38.427Z",
    "createdAt": "2019-12-19T04:18:38.427Z"
}
```

#### GET ALL TRANSACTIONS

```
curl -X GET http://localhost:5000/api/1/transactions/
```

```
STATUS: 200

[
    {
        "cardNumber": "xxxx-xxxx-xxxx-2345",
        "id": 1,
        "description": "Big Mac",
        "amount": "32.00",
        "method": "debit_card",
        "cardHolder": "Ronald McDonalds",
        "cardValidity": "2019-12-16",
        "cardCVV": "381",
        "createdAt": "2019-12-19T04:18:35.934Z",
        "updatedAt": "2019-12-19T04:18:35.934Z"
    }
]
```

#### GET A SINGLE TRANSACTIONS

```
curl -X GET http://localhost:5000/api/1/transactions/1
```

```
STATUS: 200

{
    "cardNumber": "xxxx-xxxx-xxxx-2345",
    "id": 1,
    "description": "Big Mac",
    "amount": "32.00",
    "method": "debit_card",
    "cardHolder": "Ronald McDonalds",
    "cardValidity": "2019-12-16",
    "cardCVV": "381",
    "createdAt": "2019-12-19T04:18:35.934Z",
    "updatedAt": "2019-12-19T04:18:35.934Z"
}
```

### PAYABLES

#### GET ALL PAYABLES

```
curl -X GET http://localhost:5000/api/1/payables/
```

```
STATUS: 200

[
    {
        "id": 1,
        "status": "paid",
        "total": "31.04",
        "paymentDate": "2019-12-19",
        "createdAt": "2019-12-19T04:18:35.953Z",
        "updatedAt": "2019-12-19T04:18:35.953Z"
    },
    {
        "id": 2,
        "status": "waiting_funds",
        "total": "30.40",
        "paymentDate": "2020-01-18",
        "createdAt": "2019-12-19T04:40:08.002Z",
        "updatedAt": "2019-12-19T04:40:08.002Z"
    }
]
```

#### GET A SINGLE PAYABLES

```
curl -X GET http://localhost:5000/api/1/payables/1
```

```
STATUS: 200

{
    "id": 1,
    "status": "paid",
    "total": "31.04",
    "paymentDate": "2019-12-19",
    "createdAt": "2019-12-19T04:18:35.953Z",
    "updatedAt": "2019-12-19T04:18:35.953Z"
}
```

#### GET A SUMMARY OF PAYABLES

```
curl -X GET http://localhost:5000/api/1/payables/summary
```

```
STATUS: 200

[
    {
        "status": "available",
        "total": "31.04"
    },
    {
        "status": "waiting_funds",
        "total": "30.40"
    }
]

```

## NICE TO HAVE NEXT STEPS

* Implement validation and data sanitization with @happi/joi;
* Implement proper CI/CD structure to build and deploy the application;
* Implement a queue system using Bull.

## FINAL CONSIDERATIONS

As always I spent more time configuring stuff than actually coding but it was really fun nonetheless. Notice that the provided solution is meant for manage transactions of a single user since no requirements for implementing multi-user management was found. Also no deletion routes were implemented since we have not set a strong relation between the entities due to the lack of requirements.